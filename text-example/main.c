//Copyright (c) 2011-2020 <>< Charles Lohr - Under the MIT/x11 or NewBSD License you choose.
// NO WARRANTY! NO GUARANTEE OF SUPPORT! USE AT YOUR OWN RISK

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "os_generic.h"
#include <GLES3/gl3.h>
#include <asset_manager.h>
#include <asset_manager_jni.h>
#include <android_native_app_glue.h>
#include <android/sensor.h>
#include "CNFGAndroid.h"

#define CNFG_IMPLEMENTATION
#define CNFG3D
#include "CNFG.h"

float mountainangle;
float mountainoffsetx;
float mountainoffsety;

ASensorManager * sm;
const ASensor * as;
ASensorEventQueue* aeq;
ALooper * l;


void SetupIMU()
{
  sm = ASensorManager_getInstance();
  as = ASensorManager_getDefaultSensor( sm, ASENSOR_TYPE_GYROSCOPE );
  l = ALooper_prepare( ALOOPER_PREPARE_ALLOW_NON_CALLBACKS );
  aeq = ASensorManager_createEventQueue( sm, (ALooper*)&l, 0, 0, 0 ); //XXX??!?! This looks wrong.
  ASensorEventQueue_enableSensor( aeq, as);
  printf( "setEvent Rate: %d\n", ASensorEventQueue_setEventRate( aeq, as, 10000 ) );
}

float accx, accy, accz;
int accs;
int scroll_speed = 1;

#define HMX 162
#define HMY 162
short screenx, screeny;
float Heightmap[HMX*HMY];

void AccCheck()
{
  ASensorEvent evt;
  do
  {
    ssize_t s = ASensorEventQueue_getEvents( aeq, &evt, 1 );
    if( s <= 0 ) break;
    accx = evt.vector.v[0];
    accy = evt.vector.v[1];
    accz = evt.vector.v[2];
    mountainangle /*degrees*/ -= accz;// * 3.1415 / 360.0;// / 100.0;
    mountainoffsety += accy;
    mountainoffsetx += accx;
    accs++;
  } while( 1 );
}

unsigned frames = 0;
unsigned long iframeno = 0;

void AndroidDisplayKeyboard(int pShow);

int lastbuttonx = 0;
int lastbuttony = 0;
int lastmotionx = 0;
int lastmotiony = 0;
int lastbid = 0;
int lastmask = 0;
int lastkey, lastkeydown;

int scroll_posy = 0;
int posy=400;

static int keyboard_up;

int text_msg(char* msg, int x, int y, int color){
  CNFGColor( color );
  CNFGPenX = x; CNFGPenY = y;
  //  sprintf(msg);
  CNFGDrawText( msg, 10 );
  glLineWidth( 2.0 );

  return 0;
}

void HandleKey( int keycode, int bDown )
{
  lastkey = keycode;
  lastkeydown = bDown;
  if( keycode == 10 && !bDown ) { keyboard_up = 0; AndroidDisplayKeyboard( keyboard_up );  }

  if( keycode == 4 ) { AndroidSendToBack( 1 ); } //Handle Physical Back Button.

  if( keycode == 22 ) { posy++; }
  if( keycode == 21 ) { posy--; }
}

void HandleButton( int x, int y, int button, int bDown )
{
  lastbid = button;
  lastbuttonx = x;
  lastbuttony = y;

  if( bDown ) { 
    //if screen is touch at near the bottom open keyboard
  }
}

void keyboard(){
  int s = screeny - 50;
  if(lastmotiony > s){
    if(!keyboard_up){
      keyboard_up = !keyboard_up;
      AndroidDisplayKeyboard( keyboard_up );
    }
  }
}

void HandleMotion( int x, int y, int mask )
{
  lastmask = mask;
  lastmotionx = x;
  lastmotiony = y;
}

extern struct android_app * gapp;


void HandleDestroy()
{
  printf( "Destroying\n" );
  exit(10);
}

volatile int suspended;

void HandleSuspend()
{
  suspended = 1;
}

void HandleResume()
{
  suspended = 0;
}


int scroll_text(){
  int size = 15;
  scroll_posy+=scroll_speed;

  //bring back to top and double speed;
  if(scroll_posy > screeny){
    scroll_posy = -100;
    scroll_speed+=scroll_speed;
    //set speed limit
    if(scroll_speed > 10){scroll_speed = 10;}
  };
  CNFGColor( 0xFF00FF );
  CNFGPenX = 0; 
  CNFGPenY = scroll_posy;
  char st[50];
  sprintf( st, "www.FilmsByKris.com" );
  CNFGDrawText( st, size );
  glLineWidth( 2.0 );

  return 0;
}

int follow_text(){
  int size = 25;
  CNFGColor( 0xFF00FF );
  //Get cursor position
  CNFGPenX = lastmotionx; 
  CNFGPenY = lastmotiony;
  char st[50];
  sprintf( st, "LINUX" );
  CNFGDrawText( st, size );
  glLineWidth( 5.0 );

  return 0;
}

int get_date(){
  /*
     system("date > /data/data/com.filmsbykris.textExample/files/log.txt");
     FILE *fp;
     fp = fopen("/data/data/com.filmsbykris.textExample/files/log.txt","r");
     fgets(text,sizeof(text),fp);
     fclose(f);
     */

  CNFGGetDimensions( &screenx, &screeny );
  CNFGColor( 0xFFFFFF );

  CNFGPenX = 0; CNFGPenY = posy;
  char text[100];
  FILE *fp;
  fp = popen("date", "r");
  fgets(text,sizeof(text),fp);
  CNFGDrawText( text, 15 );

  return 0;
}

int data_text(){
  CNFGColor( 0xFFFF00 );
  CNFGPenX = 0; CNFGPenY = 600;
  char st[50];
  sprintf( st, "%dx%d %d %d %d %d %d %d\n%d %d\n%5.2f %5.2f %5.2f %d", screenx, screeny, lastbuttonx, lastbuttony, lastmotionx, lastmotiony, lastkey, lastkeydown, lastbid, lastmask, accx, accy, accz, accs );
  CNFGDrawText( st, 10 );
  glLineWidth( 2.0 );

  return 0;
}

int main()
{
  CNFGBGColor = 0x400000;
  CNFGSetupFullscreen( "Test Bench", 0 );

  SetupIMU();

  while(1)
  {
    iframeno++;

    CNFGHandleInput();
    AccCheck();

    if( suspended ) { usleep(50000); continue; }
    CNFGClearFrame();
    text_msg("Hello", 10, 1000, 0x00ff00);
    get_date();
    data_text();
    scroll_text();
    follow_text();

    keyboard();

    //On Android, CNFGSwapBuffers must be called, and CNFGUpdateScreenWithBitmap does not have an implied framebuffer swap.
    CNFGFlushRender();
    CNFGSwapBuffers();

  }
  return(0);
}

